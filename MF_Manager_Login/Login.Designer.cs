﻿namespace MF_Manager_Login
{
    partial class frm_login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_login = new System.Windows.Forms.Panel();
            this.btn_close = new System.Windows.Forms.Button();
            this.lbl_copyright = new System.Windows.Forms.Label();
            this.lbl_login = new System.Windows.Forms.Label();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.txt_username = new System.Windows.Forms.TextBox();
            this.lbl_lost_password = new System.Windows.Forms.Label();
            this.lbl_register = new System.Windows.Forms.Label();
            this.btn_login = new System.Windows.Forms.Button();
            this.pnl_login.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_login
            // 
            this.pnl_login.BackgroundImage = global::MF_Manager_Login.Properties.Resources.login_bg;
            this.pnl_login.Controls.Add(this.btn_close);
            this.pnl_login.Controls.Add(this.lbl_copyright);
            this.pnl_login.Controls.Add(this.lbl_login);
            this.pnl_login.Controls.Add(this.txt_password);
            this.pnl_login.Controls.Add(this.txt_username);
            this.pnl_login.Controls.Add(this.lbl_lost_password);
            this.pnl_login.Controls.Add(this.lbl_register);
            this.pnl_login.Controls.Add(this.btn_login);
            this.pnl_login.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pnl_login.Location = new System.Drawing.Point(68, 31);
            this.pnl_login.Name = "pnl_login";
            this.pnl_login.Size = new System.Drawing.Size(284, 302);
            this.pnl_login.TabIndex = 7;
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.Tomato;
            this.btn_close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_close.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_close.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_close.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_close.Location = new System.Drawing.Point(51, 223);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 32);
            this.btn_close.TabIndex = 21;
            this.btn_close.Text = "Close";
            this.btn_close.UseVisualStyleBackColor = false;
            // 
            // lbl_copyright
            // 
            this.lbl_copyright.AutoSize = true;
            this.lbl_copyright.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F);
            this.lbl_copyright.Location = new System.Drawing.Point(132, 38);
            this.lbl_copyright.Name = "lbl_copyright";
            this.lbl_copyright.Size = new System.Drawing.Size(149, 7);
            this.lbl_copyright.TabIndex = 20;
            this.lbl_copyright.Text = "Bsc(Hons) Computing   ||  Production Project";
            // 
            // lbl_login
            // 
            this.lbl_login.AutoSize = true;
            this.lbl_login.BackColor = System.Drawing.Color.MediumTurquoise;
            this.lbl_login.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold);
            this.lbl_login.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbl_login.Location = new System.Drawing.Point(156, 12);
            this.lbl_login.Name = "lbl_login";
            this.lbl_login.Size = new System.Drawing.Size(125, 22);
            this.lbl_login.TabIndex = 19;
            this.lbl_login.Text = "MF_M LOGIN";
            // 
            // txt_password
            // 
            this.txt_password.Font = new System.Drawing.Font("Source Code Pro", 8.249999F);
            this.txt_password.Location = new System.Drawing.Point(49, 192);
            this.txt_password.Multiline = true;
            this.txt_password.Name = "txt_password";
            this.txt_password.PasswordChar = '#';
            this.txt_password.Size = new System.Drawing.Size(215, 25);
            this.txt_password.TabIndex = 18;
            // 
            // txt_username
            // 
            this.txt_username.Font = new System.Drawing.Font("Source Code Pro", 8.249999F);
            this.txt_username.Location = new System.Drawing.Point(51, 127);
            this.txt_username.Multiline = true;
            this.txt_username.Name = "txt_username";
            this.txt_username.Size = new System.Drawing.Size(215, 25);
            this.txt_username.TabIndex = 17;
            // 
            // lbl_lost_password
            // 
            this.lbl_lost_password.AutoSize = true;
            this.lbl_lost_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_lost_password.Location = new System.Drawing.Point(101, 276);
            this.lbl_lost_password.Name = "lbl_lost_password";
            this.lbl_lost_password.Size = new System.Drawing.Size(106, 16);
            this.lbl_lost_password.TabIndex = 16;
            this.lbl_lost_password.Text = "Password_lost ?";
            // 
            // lbl_register
            // 
            this.lbl_register.AutoSize = true;
            this.lbl_register.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_register.Location = new System.Drawing.Point(29, 276);
            this.lbl_register.Name = "lbl_register";
            this.lbl_register.Size = new System.Drawing.Size(59, 16);
            this.lbl_register.TabIndex = 15;
            this.lbl_register.Text = "Register";
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_login.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_login.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_login.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_login.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_login.Location = new System.Drawing.Point(191, 223);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(75, 32);
            this.btn_login.TabIndex = 14;
            this.btn_login.Text = "Login";
            this.btn_login.UseVisualStyleBackColor = false;
            // 
            // frm_login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MF_Manager_Login.Properties.Resources.details__quote;
            this.ClientSize = new System.Drawing.Size(423, 367);
            this.Controls.Add(this.pnl_login);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_login";
            this.Text = "MF_Manager LOGIN";
            this.pnl_login.ResumeLayout(false);
            this.pnl_login.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_login;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Label lbl_copyright;
        private System.Windows.Forms.Label lbl_login;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.TextBox txt_username;
        private System.Windows.Forms.Label lbl_lost_password;
        private System.Windows.Forms.Label lbl_register;
        private System.Windows.Forms.Button btn_login;
    }
}

