﻿namespace MF_Manager
{
    partial class MFManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MFManager));
            this.pnl_main = new System.Windows.Forms.Panel();
            this.lbl_initial_balance = new System.Windows.Forms.Label();
            this.ms_menus = new System.Windows.Forms.MenuStrip();
            this.hOMEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mFFundamentalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fUNDSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equityFundsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.diversifiedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiCapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.largeCapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.midCapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smallCapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taxSavingFundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equityIncomeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rEPORTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.riskMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lOGOUTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnl_main.SuspendLayout();
            this.ms_menus.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_main
            // 
            this.pnl_main.BackgroundImage = global::MF_Manager.Properties.Resources.HoepZvcNVh3zCnRX7A7YC6aU;
            this.pnl_main.Controls.Add(this.lbl_initial_balance);
            this.pnl_main.Controls.Add(this.ms_menus);
            this.pnl_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_main.Location = new System.Drawing.Point(0, 0);
            this.pnl_main.Name = "pnl_main";
            this.pnl_main.Size = new System.Drawing.Size(984, 661);
            this.pnl_main.TabIndex = 7;
            // 
            // lbl_initial_balance
            // 
            this.lbl_initial_balance.AutoSize = true;
            this.lbl_initial_balance.Location = new System.Drawing.Point(892, 0);
            this.lbl_initial_balance.Name = "lbl_initial_balance";
            this.lbl_initial_balance.Size = new System.Drawing.Size(0, 13);
            this.lbl_initial_balance.TabIndex = 1;
            // 
            // ms_menus
            // 
            this.ms_menus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hOMEToolStripMenuItem,
            this.fUNDSToolStripMenuItem,
            this.rEPORTToolStripMenuItem,
            this.lOGOUTToolStripMenuItem});
            this.ms_menus.Location = new System.Drawing.Point(0, 0);
            this.ms_menus.Name = "ms_menus";
            this.ms_menus.Size = new System.Drawing.Size(984, 24);
            this.ms_menus.TabIndex = 0;
            this.ms_menus.Text = "menuStrip1";
            // 
            // hOMEToolStripMenuItem
            // 
            this.hOMEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mFFundamentalsToolStripMenuItem});
            this.hOMEToolStripMenuItem.Image = global::MF_Manager.Properties.Resources.if_JD_15_22242711;
            this.hOMEToolStripMenuItem.Name = "hOMEToolStripMenuItem";
            this.hOMEToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.hOMEToolStripMenuItem.Text = "HOME";
            // 
            // mFFundamentalsToolStripMenuItem
            // 
            this.mFFundamentalsToolStripMenuItem.Image = global::MF_Manager.Properties.Resources.if_link_basic_blue_69523;
            this.mFFundamentalsToolStripMenuItem.Name = "mFFundamentalsToolStripMenuItem";
            this.mFFundamentalsToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.mFFundamentalsToolStripMenuItem.Text = "MF Fundamentals";
            // 
            // fUNDSToolStripMenuItem
            // 
            this.fUNDSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.equityFundsToolStripMenuItem});
            this.fUNDSToolStripMenuItem.Image = global::MF_Manager.Properties.Resources.if_Money_Increase_379473;
            this.fUNDSToolStripMenuItem.Name = "fUNDSToolStripMenuItem";
            this.fUNDSToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.fUNDSToolStripMenuItem.Text = "FUNDS";
            // 
            // equityFundsToolStripMenuItem
            // 
            this.equityFundsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.diversifiedToolStripMenuItem});
            this.equityFundsToolStripMenuItem.Name = "equityFundsToolStripMenuItem";
            this.equityFundsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.equityFundsToolStripMenuItem.Text = "Equity Funds";
            // 
            // diversifiedToolStripMenuItem
            // 
            this.diversifiedToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.multiCapToolStripMenuItem,
            this.largeCapToolStripMenuItem,
            this.midCapToolStripMenuItem,
            this.smallCapToolStripMenuItem,
            this.taxSavingFundToolStripMenuItem,
            this.equityIncomeToolStripMenuItem});
            this.diversifiedToolStripMenuItem.Name = "diversifiedToolStripMenuItem";
            this.diversifiedToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.diversifiedToolStripMenuItem.Text = "Diversified";
            // 
            // multiCapToolStripMenuItem
            // 
            this.multiCapToolStripMenuItem.Name = "multiCapToolStripMenuItem";
            this.multiCapToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.multiCapToolStripMenuItem.Text = "Multi-Cap";
            // 
            // largeCapToolStripMenuItem
            // 
            this.largeCapToolStripMenuItem.Name = "largeCapToolStripMenuItem";
            this.largeCapToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.largeCapToolStripMenuItem.Text = "Large Cap";
            // 
            // midCapToolStripMenuItem
            // 
            this.midCapToolStripMenuItem.Name = "midCapToolStripMenuItem";
            this.midCapToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.midCapToolStripMenuItem.Text = "Mid Cap";
            // 
            // smallCapToolStripMenuItem
            // 
            this.smallCapToolStripMenuItem.Name = "smallCapToolStripMenuItem";
            this.smallCapToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.smallCapToolStripMenuItem.Text = "Small Cap";
            // 
            // taxSavingFundToolStripMenuItem
            // 
            this.taxSavingFundToolStripMenuItem.Name = "taxSavingFundToolStripMenuItem";
            this.taxSavingFundToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.taxSavingFundToolStripMenuItem.Text = "Tax Saving Fund";
            // 
            // equityIncomeToolStripMenuItem
            // 
            this.equityIncomeToolStripMenuItem.Name = "equityIncomeToolStripMenuItem";
            this.equityIncomeToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.equityIncomeToolStripMenuItem.Text = "Equity Income";
            // 
            // rEPORTToolStripMenuItem
            // 
            this.rEPORTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.riskMatrixToolStripMenuItem});
            this.rEPORTToolStripMenuItem.Image = global::MF_Manager.Properties.Resources.if_custom_reports_63120;
            this.rEPORTToolStripMenuItem.Name = "rEPORTToolStripMenuItem";
            this.rEPORTToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.rEPORTToolStripMenuItem.Text = "REPORT";
            // 
            // riskMatrixToolStripMenuItem
            // 
            this.riskMatrixToolStripMenuItem.Image = global::MF_Manager.Properties.Resources.if_flag_red_36057;
            this.riskMatrixToolStripMenuItem.Name = "riskMatrixToolStripMenuItem";
            this.riskMatrixToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.riskMatrixToolStripMenuItem.Text = "Risk Matrix";
            this.riskMatrixToolStripMenuItem.Click += new System.EventHandler(this.riskMatrixToolStripMenuItem_Click);
            // 
            // lOGOUTToolStripMenuItem
            // 
            this.lOGOUTToolStripMenuItem.Image = global::MF_Manager.Properties.Resources.if_logout_173050;
            this.lOGOUTToolStripMenuItem.Name = "lOGOUTToolStripMenuItem";
            this.lOGOUTToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.lOGOUTToolStripMenuItem.Text = "LOGOUT";
            this.lOGOUTToolStripMenuItem.Click += new System.EventHandler(this.lOGOUTToolStripMenuItem_Click);
            // 
            // MFManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 661);
            this.Controls.Add(this.pnl_main);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.ms_menus;
            this.Name = "MFManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MF_Manager";
            this.Load += new System.EventHandler(this.MFManager_Load);
            this.pnl_main.ResumeLayout(false);
            this.pnl_main.PerformLayout();
            this.ms_menus.ResumeLayout(false);
            this.ms_menus.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_main;
        private System.Windows.Forms.MenuStrip ms_menus;
        private System.Windows.Forms.ToolStripMenuItem hOMEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rEPORTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lOGOUTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem riskMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mFFundamentalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fUNDSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equityFundsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem diversifiedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiCapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem largeCapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem midCapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smallCapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taxSavingFundToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equityIncomeToolStripMenuItem;
        private System.Windows.Forms.Label lbl_initial_balance;
    }
}

