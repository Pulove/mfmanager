﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MF_Manager
{
    public partial class Login_form : Form
    {
        public Login_form()
        {
            InitializeComponent();
        }

        private void btn_login_Click(object sender, EventArgs e)
        {

            //storing the user inserted value in a variable
            string username = txt_username.Text;
            string password = txt_password.Text;


            //validation of the authorized user for null
            if (username == "")
            {
                MessageBox.Show("Please enter the username");
                txt_username.Focus();

            }
            //
            else if (password == "")
            {
                MessageBox.Show("Please enter the password");
                txt_password.Focus();
            }
            //process after the correct authorization details has been provided 
            else {
                if (username == "admin" && password == "admin")
                {

                    MFManager mf = new MFManager();
                    this.Hide();
                  //  mf.ShowDialog();
                    mf.Show();

                }
                else {
                    MessageBox.Show("Username and password incorrect");
                }
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
