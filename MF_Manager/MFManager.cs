﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MF_Manager
{
    public partial class MFManager : Form
    {  
        public MFManager()
        {
            InitializeComponent();
        }
        //variable to hold initial balance
        public string trial_balance;

        private void riskMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RiskMatrix rm = new RiskMatrix();
            pnl_main.Controls.Clear();
            pnl_main.Controls.Add(rm);
        }

        private void fUNDSToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
        }

        private void lOGOUTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login_form lf = new Login_form();
            this.Hide();
            lf.ShowDialog();
            this.Close();
        }

        private void MFManager_Load(object sender, EventArgs e)
        {
            Random initial_trial_balance = new Random();
            int randomValue = initial_trial_balance.Next(500, 50000);
            lbl_initial_balance.Text = randomValue.ToString();
            MessageBox.Show("You have total of" + randomValue.ToString() + "Dollars ($$) to invest");

        }
    }
}
