﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Access_MF_Manager
{
   public class Data_Access_Operation
    {
        public static string connection_string = "Data Source=RAAVAN\\sSQLSERVER;Initial Catalog = MFManager; Integrated Security = True";
        public static SqlConnection GetConnection() {
            SqlConnection connection = new SqlConnection(connection_string);
            if (connection.State != System.Data.ConnectionState.Open) {
                connection.Open();
            }
            return connection;
        }
    }
}
